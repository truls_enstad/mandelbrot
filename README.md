# mandelbrot

## Generate your own images of the mandelbrot set
Explore the endless complexity and depth of the mandelbrot set with this
python program. You only need to specify the min and max values of x and y, and the program does the rest.

## images generated by the program

![mandelbrot](mandelbrot.png)

![mandelbrot_ex](mandelbrot_ex.png)
