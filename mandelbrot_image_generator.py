import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import colors
from numba import jit

@jit
def mandelbrot(z, n):
    c = z
    for n in range(n):
        if abs(z) > 2:
            return n
        z = z * z + c
    return n

@jit
def mandelbrot_set(xmin, xmax, ymin, ymax, width, height, n):
    x1 = np.linspace(xmin, xmax, width)
    x2 = np.linspace(ymin, ymax, height)
    n3 = np.empty((width, height))
    for i in range(width):
        for j in range(height):
            n3[i, j] = mandelbrot(x1[i] + 1j * x2[j], n)

    return x1, x2, n3

def save_image(fig):
    filename = "mandelbrot.png"
    fig.savefig(filename)

def mandelbrot_image(xmin, xmax, ymin, ymax, width=20, height=20, maxiter=256, cmap='jet', dpi=72):
    img_width = dpi * width
    img_height = dpi * height
    x, y, z = mandelbrot_set(xmin, xmax, ymin, ymax, img_width, img_height, maxiter)

    fig, ax = plt.subplots(figsize=(width, height), dpi=72)
    ticks = np.arange(0, img_width, 3*dpi)
    x_ticks = xmin + (xmax-xmin)*ticks/img_width
    plt.xticks(ticks, x_ticks)
    y_ticks = ymin + (ymax-ymin)*ticks/img_width
    plt.yticks(ticks, y_ticks)

    ax.imshow(z.T, cmap=cmap, origin='lower')

    return fig


save_image(mandelbrot_image(-2.5, 1.5, -2.0, 2.0, maxiter=256))
